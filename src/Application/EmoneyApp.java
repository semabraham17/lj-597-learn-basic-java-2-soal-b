package Application;

import Data.*;

public class EmoneyApp {
    public static void main(String[] args) {
        AkunEmoney akun5 = new AkunEmoney(201);
        AkunEmoney akun6 = new AkunEmoney(202);

//        untuk testing, saldo di hardcode 100.000
        akun5.tambahSaldo(100_000);

        MesinEmoney mesin1 = new MesinEmoney();
        mesin1.run();
    }
}
