package Application;

import Data.*;

public class AtmApp {
    public static void main(String[] args) {
        AkunAtm akun1 = new AkunAtm(1001,101,1234 ,"eko","cb1");
        AkunAtm akun2 = new AkunAtm(1002,102,1234,"eko","cb1");
        AkunAtm akun3 = new AkunAtm(1003,103,1234,"eko","cb1");
        AkunAtm akun4 = new AkunAtm(1004,104,1234,"eko","cb1");

        AkunEmoney akun5 = new AkunEmoney(201);
        AkunEmoney akun6 = new AkunEmoney(202);

        MesinAtm atm1 = new MesinAtm();
        atm1.run();

//        jika ingin dilanjutkan dengan aplikasi Emoney

//        akun5.tambahSaldo(10000);
        MesinEmoney mesin1 = new MesinEmoney();
        mesin1.run();
    }

}
