package Data;

import java.util.ArrayList;

public class AkunAtm extends Akun{
    static public ArrayList<AkunAtm> listAkun = new ArrayList<>();
    int nomorRekening;
    int pin;
    String namaPemilik;
    String namaKCP;

    public AkunAtm(int nomorRekening, int noKartu, int pin, String namaPemilik, String namaKCP) {
        this.nomorRekening = nomorRekening;
        this.pin = pin;
        this.nomorKartu = noKartu;
        this.namaPemilik = namaPemilik;
        this.namaKCP = namaKCP;
        listAkun.add(this);
    }

    static public AkunAtm findAkunByRekening(int rekening){
        for (AkunAtm akun: listAkun){
            if (akun.nomorRekening == rekening) return akun;
        }
        return null;
    }

    static public AkunAtm findAkunByNokartu(int noKartu){
        for (AkunAtm akun: listAkun){
            if (akun.nomorKartu == noKartu) return akun;
        }
        return null;
    }

    public AkunAtm getAkunByRekening (int rekening){
        System.out.println("ditemukan");
        AkunAtm akun = getAkunByRekening(rekening);
        System.out.println(akun);
        return akun;
    }

    public int getPin() {
        return pin;
    }
}
