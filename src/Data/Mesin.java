package Data;

import java.util.Scanner;

public class Mesin {
    public static int getInput(){
        Scanner input = new Scanner(System.in);
        try{
            return Integer.parseInt(input.nextLine());
        }
        catch(NumberFormatException e){
            System.out.println("input salah, masukan lagi");
            return getInput();
        }
    }

    public void kembali(){
        System.out.println("tekan enter untuk kembali");
        try {
            System.in.read();
        }
        catch(Exception e)
        {}
    }

}
