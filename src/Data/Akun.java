package Data;

import java.util.ArrayList;

public class Akun {

    final boolean success = true;
    int nomorKartu;
    int saldo;

    ArrayList<Transaksi> listMutasi = new ArrayList<>();


    public int getSaldo() {
        return saldo;
    }

    public int getNomorKartu() {
        return nomorKartu;
    }

    public ArrayList<Transaksi> getMutasi() {
        return listMutasi;
    }

    public boolean potongSaldo(int jumlah) {

        if (saldo < jumlah) return !success;

        saldo -= jumlah;
        Transaksi transaksi = new Transaksi("kredit", jumlah);
        listMutasi.add(transaksi);
        return success;
    }

    public boolean tambahSaldo(int jumlah) {
        saldo += jumlah;
        Transaksi transaksi = new Transaksi("debit", jumlah);
        System.out.println(transaksi);
        listMutasi.add(transaksi);
        return success;
    }



}
