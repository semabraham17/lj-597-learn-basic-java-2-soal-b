package Data;

import Data.Kartu;

public class KartuAtm extends Kartu {

    int noRekening;
    String namaPemilik;
    String namaKCP;

    public KartuAtm(int noKartu, int noRekening, String namaPemilik, String namaKCP) {

        this.nomorKartu = noKartu;
        this.noRekening = noRekening;
        this.namaPemilik = namaPemilik;
        this.namaKCP = namaKCP;
    }
}

