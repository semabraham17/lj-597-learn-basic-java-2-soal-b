package Data;

import java.util.ArrayList;

public class MesinAtm extends Mesin{
    boolean autenticated = false;
    boolean stoped = false;

    AkunAtm akun;


    public void run(){
        while (!this.stoped){
            displayWelcome();

            System.out.println("\nmasukan no Kartu Atm");
            int noKartu = getInput();

            System.out.println("\nmasukan pin");
            int pin = getInput();

            this.autenticated = getOtentifikasi(noKartu,pin);

            while (this.autenticated){
                displayMenu();

                int inputMenu = getInput();
                goToMenuByInput(inputMenu);
            }
        }
    }
    public boolean getOtentifikasi(int noKartu, int pin){
        AkunAtm akun = AkunAtm.findAkunByNokartu(noKartu);
        if (akun == null ){
            System.out.println("\nkartu tidak ditemukan");
            return false;
        }

        if (pin == akun.getPin()){
            System.out.println("\nterotentifikasi");
            this.akun = akun;
            return true;
        }
        else {
            System.out.println("\npin yang anda masukan salah");
            return false;
        }
    }


    private void goToMenuByInput(int input) {
        switch (input) {
            case 1 :
                displayInformasiSaldo();
                kembali();
                break;
            case 2 :
                setorTunai();
                kembali();
                break;
            case 3 :
                bayar();
                kembali();
                break;
            case 4 :
                transfer();
                kembali();
                break;
            case 5 :
                tarikTunai();
                kembali();
                break;
            case 6 :
                topUpEmoney();
                kembali();
                break;
            case 7 :
                displayMutasi();
                kembali();
                break;
            case 8 :
                logOut();
                break;
            case 9 :
                keluar();
                break;
        }
    }

    private void displayWelcome() {
        System.out.println("\nSelamaat datang di ATM App");
    }

    private void displayMenu(){
        System.out.println("\nMenu mesin ATM");
        System.out.println("1. Informasi Saldo");
        System.out.println("2. Setor Tunai");
        System.out.println("3. Pembayaran");
        System.out.println("4. Transfer");
        System.out.println("5. Tarik Tunai");
        System.out.println("6. TopUp Emoney");
        System.out.println("7. Info Mutasi");
        System.out.println("8. Logout");
        System.out.println("9. selesaikan program");

    }

    public void displayInformasiSaldo() {
        int saldo = akun.getSaldo();
        System.out.println("\nsaldo Anda : "+ saldo);
    }

    public void setorTunai(){
        System.out.println("\nmasukan jumlah yang akan di setor ");
        int jumlah = getInput();
        this.akun.tambahSaldo(jumlah);
        System.out.println("\nsetoran Anda diterima sejumlah : " + jumlah);
        System.out.println("saldo anda menjadi : " + akun.getSaldo());
    }

    public void bayar() {
        System.out.println("\nmasukan no pembayaran");
        int noPembayaran = getInput();

        System.out.println("\nmasukan jumlah");
        int jumlah = getInput();

        boolean success = this.akun.potongSaldo(jumlah);
        if (success) System.out.println("pembayaran ke no " + noPembayaran + " berhasil");
        else System.out.println("transaksi gagal, saldo kurang");
    }

    public void transfer() {
        System.out.println("\nmasukan no rekening");
        int noRekening = getInput();

        System.out.println("\nmasukan jumlah");
        int jumlah = getInput();

        AkunAtm akunRekeningTujuan;
        akunRekeningTujuan = AkunAtm.findAkunByRekening(noRekening);

        if (akunRekeningTujuan != null) {
            akunRekeningTujuan.tambahSaldo(jumlah);

            boolean success = this.akun.potongSaldo(jumlah);
            if (success) System.out.println("transfer ke rekening " + noRekening + " berhasil");
            else System.out.println("transaksi gagal, saldo kurang");

        }
        else {
            System.out.println("transfer gagal, nomor rekening tidak ditemukan");
        }
    }

    public void tarikTunai() {
        System.out.println("masukan nominal ");
        int nominal = getInput();

        boolean succes = this.akun.potongSaldo(nominal);
        if (!succes){
            System.out.println("tarik tunai gagal - saldo yang anda miliki kurang");
            return;
        }
        System.out.println("\nAtm Mengeluarkan uang "+ nominal + " tarik tunai berhasil");
        System.out.println("saldo anda menjadi : " + akun.getSaldo());
    }

    public void topUpEmoney() {

        System.out.println("\nmasukan no kartu");
        int noKartu = getInput();

        AkunEmoney akun = AkunEmoney.findAkunByNoKartu(noKartu);
        if (akun == null) {
            System.out.println("\nkartu tidak ditemukan");
            return;
        }
        System.out.println("\nmasukan nominal topup");
        int jumlah = getInput();

        boolean success = this.akun.potongSaldo(jumlah);
        if (!success){
            System.out.println("topup gagal - saldo yang anda miliki kurang");
            return;
        }
        boolean EmoneySuccess = akun.tambahSaldo(jumlah);
        if (!EmoneySuccess){
            System.out.println("transaksi dibatalkan - saldo maksimal 1jt");
            this.akun.tambahSaldo(jumlah);
            return;
        }
        System.out.println("top up sukses");
    }

    public void displayMutasi() {
        System.out.println("\nTransaksi anda");
        ArrayList<Transaksi> listTransaksi = akun.getMutasi();
        for (Transaksi transaksi : listTransaksi){
            System.out.println("tanggal : " + transaksi.tanggal + " " + transaksi.jenis + " " + transaksi.jumlah);
        }
    }

    public void logOut(){
        this.autenticated = false;
    }

    public void keluar(){
        logOut();
        this.stoped = true;
    }

}
