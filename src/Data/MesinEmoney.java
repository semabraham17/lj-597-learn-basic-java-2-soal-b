package Data;

import Data.AkunEmoney;
import Data.Kartu;

import java.util.ArrayList;

public class MesinEmoney extends Mesin{

    AkunEmoney akun;
    boolean stoped = false;
    boolean autenticated = false;

    public void run(){
        while (!this.stoped){
            displayWelcome();

            System.out.println("masukan no Kartu Emoney");
            int noKartu = getInput();

            getOtentifikasi(noKartu);

            while (autenticated){
                displayMenu();

                int inputMenu = getInput();
                goToMenuByInput(inputMenu);
            }
        }

    }
    public boolean getOtentifikasi(int noKartu) {
        AkunEmoney akun = AkunEmoney.findAkunByNoKartu(noKartu);
        this.akun = akun;

        if (akun == null) {
            System.out.println("kartu tidak ditemukan");
            return false;
        }
        this.autenticated = true;
        return true;
    }

    private void displayWelcome() {
        System.out.println("selamaat datang di Emoney App");
    }

    private void displayMenu(){
        System.out.println("menu mesin Emoney");
        System.out.println("1. Informasi Saldo");
        System.out.println("2. Pembayaran");
        System.out.println("3. selesaikan program");
    }

    private void goToMenuByInput(int input) {
        switch (input) {
            case 1 :
                displayInformasiSaldo();
                kembali();
                break;
            case 2 :
                bayar();
                kembali();
                break;
            case 3 :
                this.autenticated=false;
                this.stoped=true;
                break;

        }
    }


    public void bayar() {
        System.out.println("\nmasukan no pembayaran");
        int noPembayaran = getInput();

        System.out.println("\nmasukan jumlah");
        int jumlah = getInput();

        boolean success = this.akun.potongSaldo(jumlah);
        if (success) System.out.println("pembayaran ke no " + noPembayaran + " berhasil");
        else System.out.println("transaksi gagal, saldo kurang");
    }

    public int displayInformasiSaldo() {
        int saldo = akun.getSaldo();
        System.out.println("\nsaldo Anda : "+ saldo);
        return saldo;
    }

}
