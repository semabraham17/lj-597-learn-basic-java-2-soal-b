package Data;

import java.time.LocalDateTime;

public class Transaksi {
    int jumlah;
    LocalDateTime tanggal;
    String jenis;

    public Transaksi(String jenis, int jumlah) {
        this.jumlah = jumlah;
        this.jenis = jenis;
        this.tanggal = LocalDateTime.now();
    }
}
