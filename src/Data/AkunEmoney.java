package Data;

import java.util.ArrayList;

public class AkunEmoney extends Akun{

    static public ArrayList<AkunEmoney> listAkun = new ArrayList<>();
    static private final int MAX_BALANCE = 1000000;
    public AkunEmoney (int nomorKartu) {
        this.nomorKartu = nomorKartu;
        listAkun.add(this);
    }

    static public AkunEmoney findAkunByNoKartu(int noKartu){
        for (AkunEmoney akun: listAkun){
            if (akun.nomorKartu == noKartu) return akun;
        }
        return null;
    }

    @Override
    public boolean tambahSaldo(int jumlah) {
        if (saldo + jumlah > MAX_BALANCE) return !success;

        saldo += jumlah;
        Transaksi transaksi = new Transaksi("debit", jumlah);
        System.out.println(transaksi);
        listMutasi.add(transaksi);
        return success;
    }



}
