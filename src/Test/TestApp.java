package Test;

import java.util.Scanner;

public class TestApp {
    public static void main(String[] args) {
        System.out.println("masukan sebuah bilangan");
        int bilangan = getInput();
        System.out.println("bilangan yang di inputkan adalah " + bilangan);
    }
    public static int getInput(){
        Scanner input = new Scanner(System.in);
        try{
            return Integer.parseInt(input.nextLine());
        }
        catch(NumberFormatException e){
            System.out.println("input salah, masukan lagi");
            return getInput();
        }
    }
}
